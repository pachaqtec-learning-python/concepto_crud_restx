from flask import Flask
from flask_restx import Api

from controller.ContactController import ContactController
from controller.ProductController import ProductController

app = Flask(__name__)
api = Api(app)

api.add_resource(ContactController, "/contact")
api.add_resource(ProductController, "/product")

if __name__ == '__main__':
  app.run(port=8000, debug=True)
