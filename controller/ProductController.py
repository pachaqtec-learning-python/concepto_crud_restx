from flask_restx import Resource

# CRUD
class ProductController(Resource):
    # CREAR PRODUCTO
    def post(self):
        return 'post', 201

    def get(self):
        return 'get', 200

    def put(self):
        return 'put'

    def delete(self):
        return 'delete', 204